/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.SourceRepository;

public class SourceRepositoriesClient {
    private ApiClient apiClient;

    public SourceRepositoriesClient() {
        this.apiClient = new ApiClient().setUserAgent("SourceRepositoriesClient 1.0.0-RC-SNAPSHOT");
    }

    public SourceRepositoriesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public SourceRepositoriesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addSourceRepository
     * @param sourcerepository SourceRepository object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addSourceRepositoryCall(SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = sourcerepository;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addSourceRepositoryValidateBeforeCall(SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourcerepository' is set
        if (sourcerepository == null) {
            throw new ApiException("Missing the required parameter 'sourcerepository' when calling addSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addSourceRepositoryCall(sourcerepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new sourcerepository
     * 
     * @param sourcerepository SourceRepository object to be created (required)
     * @return SourceRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepository addSourceRepository(SourceRepository sourcerepository) throws ApiException {
        ApiResponse<SourceRepository> resp = addSourceRepositoryWithHttpInfo(sourcerepository);
        return resp.getData();
    }

    /**
     * Add a new sourcerepository
     * 
     * @param sourcerepository SourceRepository object to be created (required)
     * @return ApiResponse&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepository> addSourceRepositoryWithHttpInfo(SourceRepository sourcerepository) throws ApiException {
        com.squareup.okhttp.Call call = addSourceRepositoryValidateBeforeCall(sourcerepository, null, null);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new sourcerepository (asynchronously)
     * 
     * @param sourcerepository SourceRepository object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addSourceRepositoryAsync(SourceRepository sourcerepository, final ApiCallback<SourceRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addSourceRepositoryValidateBeforeCall(sourcerepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteSourceRepository
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteSourceRepositoryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteSourceRepositoryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteSourceRepositoryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteSourceRepository(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteSourceRepositoryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteSourceRepositoryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteSourceRepositoryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered sourcerepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteSourceRepositoryAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteSourceRepositoryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getSourceRepositories
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoriesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getSourceRepositoriesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getSourceRepositoriesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible sourcerepositories for authenticated user
     * 
     * @return List&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<SourceRepository> getSourceRepositories() throws ApiException {
        ApiResponse<List<SourceRepository>> resp = getSourceRepositoriesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible sourcerepositories for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;SourceRepository&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<SourceRepository>> getSourceRepositoriesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getSourceRepositoriesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<SourceRepository>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible sourcerepositories for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoriesAsync(final ApiCallback<List<SourceRepository>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getSourceRepositoriesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<SourceRepository>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getSourceRepository
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getSourceRepositoryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getSourceRepositoryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific sourcerepository object
     * 
     * @param resourceId resource's id (required)
     * @return SourceRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepository getSourceRepository(String resourceId) throws ApiException {
        ApiResponse<SourceRepository> resp = getSourceRepositoryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific sourcerepository object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepository> getSourceRepositoryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getSourceRepositoryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific sourcerepository object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoryAsync(String resourceId, final ApiCallback<SourceRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getSourceRepositoryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateSourceRepository
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateSourceRepositoryCall(String resourceId, SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = sourcerepository;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateSourceRepositoryValidateBeforeCall(String resourceId, SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateSourceRepository(Async)");
        }
        
        // verify the required parameter 'sourcerepository' is set
        if (sourcerepository == null) {
            throw new ApiException("Missing the required parameter 'sourcerepository' when calling updateSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateSourceRepositoryCall(resourceId, sourcerepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @return SourceRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepository updateSourceRepository(String resourceId, SourceRepository sourcerepository) throws ApiException {
        ApiResponse<SourceRepository> resp = updateSourceRepositoryWithHttpInfo(resourceId, sourcerepository);
        return resp.getData();
    }

    /**
     * Fully update a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @return ApiResponse&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepository> updateSourceRepositoryWithHttpInfo(String resourceId, SourceRepository sourcerepository) throws ApiException {
        com.squareup.okhttp.Call call = updateSourceRepositoryValidateBeforeCall(resourceId, sourcerepository, null, null);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered sourcerepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateSourceRepositoryAsync(String resourceId, SourceRepository sourcerepository, final ApiCallback<SourceRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateSourceRepositoryValidateBeforeCall(resourceId, sourcerepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateSourceRepositoryAttributes
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateSourceRepositoryAttributesCall(String resourceId, SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = sourcerepository;
        
        // create path and map variables
        String localVarPath = "/sourcerepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateSourceRepositoryAttributesValidateBeforeCall(String resourceId, SourceRepository sourcerepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateSourceRepositoryAttributes(Async)");
        }
        
        // verify the required parameter 'sourcerepository' is set
        if (sourcerepository == null) {
            throw new ApiException("Missing the required parameter 'sourcerepository' when calling updateSourceRepositoryAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateSourceRepositoryAttributesCall(resourceId, sourcerepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @return SourceRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepository updateSourceRepositoryAttributes(String resourceId, SourceRepository sourcerepository) throws ApiException {
        ApiResponse<SourceRepository> resp = updateSourceRepositoryAttributesWithHttpInfo(resourceId, sourcerepository);
        return resp.getData();
    }

    /**
     * Partially update a registered sourcerepository
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @return ApiResponse&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepository> updateSourceRepositoryAttributesWithHttpInfo(String resourceId, SourceRepository sourcerepository) throws ApiException {
        com.squareup.okhttp.Call call = updateSourceRepositoryAttributesValidateBeforeCall(resourceId, sourcerepository, null, null);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered sourcerepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param sourcerepository SourceRepository object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateSourceRepositoryAttributesAsync(String resourceId, SourceRepository sourcerepository, final ApiCallback<SourceRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateSourceRepositoryAttributesValidateBeforeCall(resourceId, sourcerepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
