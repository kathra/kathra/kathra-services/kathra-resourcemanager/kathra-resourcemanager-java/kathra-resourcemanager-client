/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.BinaryRepository;

public class BinaryRepositoriesClient {
    private ApiClient apiClient;

    public BinaryRepositoriesClient() {
        this.apiClient = new ApiClient().setUserAgent("BinaryRepositoriesClient 1.0.0-RC-SNAPSHOT");
    }

    public BinaryRepositoriesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public BinaryRepositoriesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addBinaryRepository
     * @param binaryrepository BinaryRepository object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryCall(BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryrepository;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addBinaryRepositoryValidateBeforeCall(BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'binaryrepository' is set
        if (binaryrepository == null) {
            throw new ApiException("Missing the required parameter 'binaryrepository' when calling addBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addBinaryRepositoryCall(binaryrepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new binaryrepository
     * 
     * @param binaryrepository BinaryRepository object to be created (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository addBinaryRepository(BinaryRepository binaryrepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = addBinaryRepositoryWithHttpInfo(binaryrepository);
        return resp.getData();
    }

    /**
     * Add a new binaryrepository
     * 
     * @param binaryrepository BinaryRepository object to be created (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> addBinaryRepositoryWithHttpInfo(BinaryRepository binaryrepository) throws ApiException {
        com.squareup.okhttp.Call call = addBinaryRepositoryValidateBeforeCall(binaryrepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new binaryrepository (asynchronously)
     * 
     * @param binaryrepository BinaryRepository object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addBinaryRepositoryAsync(BinaryRepository binaryrepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addBinaryRepositoryValidateBeforeCall(binaryrepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteBinaryRepository
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteBinaryRepositoryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteBinaryRepositoryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteBinaryRepositoryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteBinaryRepository(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteBinaryRepositoryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteBinaryRepositoryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteBinaryRepositoryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered binaryrepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteBinaryRepositoryAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteBinaryRepositoryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBinaryRepositories
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoriesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBinaryRepositoriesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getBinaryRepositoriesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible binaryrepositories for authenticated user
     * 
     * @return List&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<BinaryRepository> getBinaryRepositories() throws ApiException {
        ApiResponse<List<BinaryRepository>> resp = getBinaryRepositoriesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible binaryrepositories for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;BinaryRepository&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<BinaryRepository>> getBinaryRepositoriesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getBinaryRepositoriesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<BinaryRepository>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible binaryrepositories for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoriesAsync(final ApiCallback<List<BinaryRepository>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBinaryRepositoriesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<BinaryRepository>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBinaryRepository
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBinaryRepositoryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBinaryRepositoryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific binaryrepository object
     * 
     * @param resourceId resource's id (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository getBinaryRepository(String resourceId) throws ApiException {
        ApiResponse<BinaryRepository> resp = getBinaryRepositoryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific binaryrepository object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> getBinaryRepositoryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getBinaryRepositoryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific binaryrepository object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBinaryRepositoryAsync(String resourceId, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBinaryRepositoryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateBinaryRepository
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryCall(String resourceId, BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryrepository;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateBinaryRepositoryValidateBeforeCall(String resourceId, BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateBinaryRepository(Async)");
        }
        
        // verify the required parameter 'binaryrepository' is set
        if (binaryrepository == null) {
            throw new ApiException("Missing the required parameter 'binaryrepository' when calling updateBinaryRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateBinaryRepositoryCall(resourceId, binaryrepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository updateBinaryRepository(String resourceId, BinaryRepository binaryrepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = updateBinaryRepositoryWithHttpInfo(resourceId, binaryrepository);
        return resp.getData();
    }

    /**
     * Fully update a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> updateBinaryRepositoryWithHttpInfo(String resourceId, BinaryRepository binaryrepository) throws ApiException {
        com.squareup.okhttp.Call call = updateBinaryRepositoryValidateBeforeCall(resourceId, binaryrepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered binaryrepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAsync(String resourceId, BinaryRepository binaryrepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateBinaryRepositoryValidateBeforeCall(resourceId, binaryrepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateBinaryRepositoryAttributes
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAttributesCall(String resourceId, BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = binaryrepository;
        
        // create path and map variables
        String localVarPath = "/binaryrepositories/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateBinaryRepositoryAttributesValidateBeforeCall(String resourceId, BinaryRepository binaryrepository, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateBinaryRepositoryAttributes(Async)");
        }
        
        // verify the required parameter 'binaryrepository' is set
        if (binaryrepository == null) {
            throw new ApiException("Missing the required parameter 'binaryrepository' when calling updateBinaryRepositoryAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesCall(resourceId, binaryrepository, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @return BinaryRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryRepository updateBinaryRepositoryAttributes(String resourceId, BinaryRepository binaryrepository) throws ApiException {
        ApiResponse<BinaryRepository> resp = updateBinaryRepositoryAttributesWithHttpInfo(resourceId, binaryrepository);
        return resp.getData();
    }

    /**
     * Partially update a registered binaryrepository
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @return ApiResponse&lt;BinaryRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryRepository> updateBinaryRepositoryAttributesWithHttpInfo(String resourceId, BinaryRepository binaryrepository) throws ApiException {
        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesValidateBeforeCall(resourceId, binaryrepository, null, null);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered binaryrepository (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param binaryrepository BinaryRepository object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateBinaryRepositoryAttributesAsync(String resourceId, BinaryRepository binaryrepository, final ApiCallback<BinaryRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateBinaryRepositoryAttributesValidateBeforeCall(resourceId, binaryrepository, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
