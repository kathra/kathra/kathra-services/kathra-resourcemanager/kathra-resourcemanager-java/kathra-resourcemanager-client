/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.CatalogEntry;

public class CatalogEntriesClient {
    private ApiClient apiClient;

    public CatalogEntriesClient() {
        this.apiClient = new ApiClient().setUserAgent("CatalogEntriesClient 1.0.0-RC-SNAPSHOT");
    }

    public CatalogEntriesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public CatalogEntriesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addCatalogEntry
     * @param catalogentry CatalogEntry object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addCatalogEntryCall(CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentry;
        
        // create path and map variables
        String localVarPath = "/catalogentries";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addCatalogEntryValidateBeforeCall(CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'catalogentry' is set
        if (catalogentry == null) {
            throw new ApiException("Missing the required parameter 'catalogentry' when calling addCatalogEntry(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addCatalogEntryCall(catalogentry, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new catalogentry
     * 
     * @param catalogentry CatalogEntry object to be created (required)
     * @return CatalogEntry
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntry addCatalogEntry(CatalogEntry catalogentry) throws ApiException {
        ApiResponse<CatalogEntry> resp = addCatalogEntryWithHttpInfo(catalogentry);
        return resp.getData();
    }

    /**
     * Add a new catalogentry
     * 
     * @param catalogentry CatalogEntry object to be created (required)
     * @return ApiResponse&lt;CatalogEntry&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntry> addCatalogEntryWithHttpInfo(CatalogEntry catalogentry) throws ApiException {
        com.squareup.okhttp.Call call = addCatalogEntryValidateBeforeCall(catalogentry, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new catalogentry (asynchronously)
     * 
     * @param catalogentry CatalogEntry object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addCatalogEntryAsync(CatalogEntry catalogentry, final ApiCallback<CatalogEntry> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addCatalogEntryValidateBeforeCall(catalogentry, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteCatalogEntry
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteCatalogEntryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentries/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteCatalogEntryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteCatalogEntry(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteCatalogEntryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteCatalogEntry(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteCatalogEntryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteCatalogEntryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteCatalogEntryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered catalogentry (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteCatalogEntryAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteCatalogEntryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntries
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntriesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentries";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntriesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getCatalogEntriesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible catalogentries for authenticated user
     * 
     * @return List&lt;CatalogEntry&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<CatalogEntry> getCatalogEntries() throws ApiException {
        ApiResponse<List<CatalogEntry>> resp = getCatalogEntriesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible catalogentries for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;CatalogEntry&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<CatalogEntry>> getCatalogEntriesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntriesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<CatalogEntry>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible catalogentries for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntriesAsync(final ApiCallback<List<CatalogEntry>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntriesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<CatalogEntry>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntry
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentries/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getCatalogEntry(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCatalogEntryCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific catalogentry object
     * 
     * @param resourceId resource's id (required)
     * @return CatalogEntry
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntry getCatalogEntry(String resourceId) throws ApiException {
        ApiResponse<CatalogEntry> resp = getCatalogEntryWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific catalogentry object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;CatalogEntry&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntry> getCatalogEntryWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific catalogentry object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryAsync(String resourceId, final ApiCallback<CatalogEntry> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateCatalogEntry
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryCall(String resourceId, CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentry;
        
        // create path and map variables
        String localVarPath = "/catalogentries/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateCatalogEntryValidateBeforeCall(String resourceId, CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateCatalogEntry(Async)");
        }
        
        // verify the required parameter 'catalogentry' is set
        if (catalogentry == null) {
            throw new ApiException("Missing the required parameter 'catalogentry' when calling updateCatalogEntry(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateCatalogEntryCall(resourceId, catalogentry, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @return CatalogEntry
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntry updateCatalogEntry(String resourceId, CatalogEntry catalogentry) throws ApiException {
        ApiResponse<CatalogEntry> resp = updateCatalogEntryWithHttpInfo(resourceId, catalogentry);
        return resp.getData();
    }

    /**
     * Fully update a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @return ApiResponse&lt;CatalogEntry&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntry> updateCatalogEntryWithHttpInfo(String resourceId, CatalogEntry catalogentry) throws ApiException {
        com.squareup.okhttp.Call call = updateCatalogEntryValidateBeforeCall(resourceId, catalogentry, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered catalogentry (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryAsync(String resourceId, CatalogEntry catalogentry, final ApiCallback<CatalogEntry> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateCatalogEntryValidateBeforeCall(resourceId, catalogentry, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateCatalogEntryAttributes
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryAttributesCall(String resourceId, CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentry;
        
        // create path and map variables
        String localVarPath = "/catalogentries/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateCatalogEntryAttributesValidateBeforeCall(String resourceId, CatalogEntry catalogentry, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateCatalogEntryAttributes(Async)");
        }
        
        // verify the required parameter 'catalogentry' is set
        if (catalogentry == null) {
            throw new ApiException("Missing the required parameter 'catalogentry' when calling updateCatalogEntryAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateCatalogEntryAttributesCall(resourceId, catalogentry, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @return CatalogEntry
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntry updateCatalogEntryAttributes(String resourceId, CatalogEntry catalogentry) throws ApiException {
        ApiResponse<CatalogEntry> resp = updateCatalogEntryAttributesWithHttpInfo(resourceId, catalogentry);
        return resp.getData();
    }

    /**
     * Partially update a registered catalogentry
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @return ApiResponse&lt;CatalogEntry&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntry> updateCatalogEntryAttributesWithHttpInfo(String resourceId, CatalogEntry catalogentry) throws ApiException {
        com.squareup.okhttp.Call call = updateCatalogEntryAttributesValidateBeforeCall(resourceId, catalogentry, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered catalogentry (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param catalogentry CatalogEntry object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryAttributesAsync(String resourceId, CatalogEntry catalogentry, final ApiCallback<CatalogEntry> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateCatalogEntryAttributesValidateBeforeCall(resourceId, catalogentry, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntry>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
