/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.Pipeline;

public class PipelinesClient {
    private ApiClient apiClient;

    public PipelinesClient() {
        this.apiClient = new ApiClient().setUserAgent("PipelinesClient 1.0.0-RC-SNAPSHOT");
    }

    public PipelinesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public PipelinesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addPipeline
     * @param pipeline Pipeline object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addPipelineCall(Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = pipeline;
        
        // create path and map variables
        String localVarPath = "/pipelines";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addPipelineValidateBeforeCall(Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'pipeline' is set
        if (pipeline == null) {
            throw new ApiException("Missing the required parameter 'pipeline' when calling addPipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addPipelineCall(pipeline, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new pipeline
     * 
     * @param pipeline Pipeline object to be created (required)
     * @return Pipeline
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Pipeline addPipeline(Pipeline pipeline) throws ApiException {
        ApiResponse<Pipeline> resp = addPipelineWithHttpInfo(pipeline);
        return resp.getData();
    }

    /**
     * Add a new pipeline
     * 
     * @param pipeline Pipeline object to be created (required)
     * @return ApiResponse&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Pipeline> addPipelineWithHttpInfo(Pipeline pipeline) throws ApiException {
        com.squareup.okhttp.Call call = addPipelineValidateBeforeCall(pipeline, null, null);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new pipeline (asynchronously)
     * 
     * @param pipeline Pipeline object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addPipelineAsync(Pipeline pipeline, final ApiCallback<Pipeline> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addPipelineValidateBeforeCall(pipeline, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deletePipeline
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deletePipelineCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deletePipelineValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deletePipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deletePipelineCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deletePipeline(String resourceId) throws ApiException {
        ApiResponse<String> resp = deletePipelineWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deletePipelineWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deletePipelineValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered pipeline (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deletePipelineAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deletePipelineValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getPipeline
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getPipelineCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getPipelineValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getPipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getPipelineCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific pipeline object
     * 
     * @param resourceId resource's id (required)
     * @return Pipeline
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Pipeline getPipeline(String resourceId) throws ApiException {
        ApiResponse<Pipeline> resp = getPipelineWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific pipeline object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Pipeline> getPipelineWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getPipelineValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific pipeline object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getPipelineAsync(String resourceId, final ApiCallback<Pipeline> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getPipelineValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getPipelines
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getPipelinesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getPipelinesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getPipelinesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible pipelines for authenticated user
     * 
     * @return List&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Pipeline> getPipelines() throws ApiException {
        ApiResponse<List<Pipeline>> resp = getPipelinesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible pipelines for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;Pipeline&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Pipeline>> getPipelinesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getPipelinesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<Pipeline>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible pipelines for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getPipelinesAsync(final ApiCallback<List<Pipeline>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getPipelinesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Pipeline>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updatePipeline
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updatePipelineCall(String resourceId, Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = pipeline;
        
        // create path and map variables
        String localVarPath = "/pipelines/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updatePipelineValidateBeforeCall(String resourceId, Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updatePipeline(Async)");
        }
        
        // verify the required parameter 'pipeline' is set
        if (pipeline == null) {
            throw new ApiException("Missing the required parameter 'pipeline' when calling updatePipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updatePipelineCall(resourceId, pipeline, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @return Pipeline
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Pipeline updatePipeline(String resourceId, Pipeline pipeline) throws ApiException {
        ApiResponse<Pipeline> resp = updatePipelineWithHttpInfo(resourceId, pipeline);
        return resp.getData();
    }

    /**
     * Fully update a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @return ApiResponse&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Pipeline> updatePipelineWithHttpInfo(String resourceId, Pipeline pipeline) throws ApiException {
        com.squareup.okhttp.Call call = updatePipelineValidateBeforeCall(resourceId, pipeline, null, null);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered pipeline (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updatePipelineAsync(String resourceId, Pipeline pipeline, final ApiCallback<Pipeline> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updatePipelineValidateBeforeCall(resourceId, pipeline, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updatePipelineAttributes
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updatePipelineAttributesCall(String resourceId, Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = pipeline;
        
        // create path and map variables
        String localVarPath = "/pipelines/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updatePipelineAttributesValidateBeforeCall(String resourceId, Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updatePipelineAttributes(Async)");
        }
        
        // verify the required parameter 'pipeline' is set
        if (pipeline == null) {
            throw new ApiException("Missing the required parameter 'pipeline' when calling updatePipelineAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updatePipelineAttributesCall(resourceId, pipeline, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @return Pipeline
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Pipeline updatePipelineAttributes(String resourceId, Pipeline pipeline) throws ApiException {
        ApiResponse<Pipeline> resp = updatePipelineAttributesWithHttpInfo(resourceId, pipeline);
        return resp.getData();
    }

    /**
     * Partially update a registered pipeline
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @return ApiResponse&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Pipeline> updatePipelineAttributesWithHttpInfo(String resourceId, Pipeline pipeline) throws ApiException {
        com.squareup.okhttp.Call call = updatePipelineAttributesValidateBeforeCall(resourceId, pipeline, null, null);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered pipeline (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param pipeline Pipeline object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updatePipelineAttributesAsync(String resourceId, Pipeline pipeline, final ApiCallback<Pipeline> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updatePipelineAttributesValidateBeforeCall(resourceId, pipeline, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
