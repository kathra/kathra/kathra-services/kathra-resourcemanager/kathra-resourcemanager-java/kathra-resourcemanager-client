/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.ImplementationVersion;

public class ImplementationVersionsClient {
    private ApiClient apiClient;

    public ImplementationVersionsClient() {
        this.apiClient = new ApiClient().setUserAgent("ImplementationVersionsClient 1.0.0-RC-SNAPSHOT");
    }

    public ImplementationVersionsClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public ImplementationVersionsClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addImplementationVersion
     * @param implementationversion ImplementationVersion object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addImplementationVersionCall(ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = implementationversion;
        
        // create path and map variables
        String localVarPath = "/implementationversions";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addImplementationVersionValidateBeforeCall(ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'implementationversion' is set
        if (implementationversion == null) {
            throw new ApiException("Missing the required parameter 'implementationversion' when calling addImplementationVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addImplementationVersionCall(implementationversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new implementationversion
     * 
     * @param implementationversion ImplementationVersion object to be created (required)
     * @return ImplementationVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ImplementationVersion addImplementationVersion(ImplementationVersion implementationversion) throws ApiException {
        ApiResponse<ImplementationVersion> resp = addImplementationVersionWithHttpInfo(implementationversion);
        return resp.getData();
    }

    /**
     * Add a new implementationversion
     * 
     * @param implementationversion ImplementationVersion object to be created (required)
     * @return ApiResponse&lt;ImplementationVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ImplementationVersion> addImplementationVersionWithHttpInfo(ImplementationVersion implementationversion) throws ApiException {
        com.squareup.okhttp.Call call = addImplementationVersionValidateBeforeCall(implementationversion, null, null);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new implementationversion (asynchronously)
     * 
     * @param implementationversion ImplementationVersion object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addImplementationVersionAsync(ImplementationVersion implementationversion, final ApiCallback<ImplementationVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addImplementationVersionValidateBeforeCall(implementationversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteImplementationVersion
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteImplementationVersionCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/implementationversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteImplementationVersionValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteImplementationVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteImplementationVersionCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteImplementationVersion(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteImplementationVersionWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteImplementationVersionWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteImplementationVersionValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered implementationversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteImplementationVersionAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteImplementationVersionValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getImplementationVersion
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getImplementationVersionCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/implementationversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getImplementationVersionValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getImplementationVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getImplementationVersionCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific implementationversion object
     * 
     * @param resourceId resource's id (required)
     * @return ImplementationVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ImplementationVersion getImplementationVersion(String resourceId) throws ApiException {
        ApiResponse<ImplementationVersion> resp = getImplementationVersionWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific implementationversion object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;ImplementationVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ImplementationVersion> getImplementationVersionWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getImplementationVersionValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific implementationversion object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getImplementationVersionAsync(String resourceId, final ApiCallback<ImplementationVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getImplementationVersionValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getImplementationVersions
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getImplementationVersionsCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/implementationversions";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getImplementationVersionsValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getImplementationVersionsCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible implementationversions for authenticated user
     * 
     * @return List&lt;ImplementationVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<ImplementationVersion> getImplementationVersions() throws ApiException {
        ApiResponse<List<ImplementationVersion>> resp = getImplementationVersionsWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible implementationversions for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;ImplementationVersion&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<ImplementationVersion>> getImplementationVersionsWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getImplementationVersionsValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<ImplementationVersion>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible implementationversions for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getImplementationVersionsAsync(final ApiCallback<List<ImplementationVersion>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getImplementationVersionsValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<ImplementationVersion>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateImplementationVersion
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateImplementationVersionCall(String resourceId, ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = implementationversion;
        
        // create path and map variables
        String localVarPath = "/implementationversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateImplementationVersionValidateBeforeCall(String resourceId, ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateImplementationVersion(Async)");
        }
        
        // verify the required parameter 'implementationversion' is set
        if (implementationversion == null) {
            throw new ApiException("Missing the required parameter 'implementationversion' when calling updateImplementationVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateImplementationVersionCall(resourceId, implementationversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @return ImplementationVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ImplementationVersion updateImplementationVersion(String resourceId, ImplementationVersion implementationversion) throws ApiException {
        ApiResponse<ImplementationVersion> resp = updateImplementationVersionWithHttpInfo(resourceId, implementationversion);
        return resp.getData();
    }

    /**
     * Fully update a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @return ApiResponse&lt;ImplementationVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ImplementationVersion> updateImplementationVersionWithHttpInfo(String resourceId, ImplementationVersion implementationversion) throws ApiException {
        com.squareup.okhttp.Call call = updateImplementationVersionValidateBeforeCall(resourceId, implementationversion, null, null);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered implementationversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateImplementationVersionAsync(String resourceId, ImplementationVersion implementationversion, final ApiCallback<ImplementationVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateImplementationVersionValidateBeforeCall(resourceId, implementationversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateImplementationVersionAttributes
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateImplementationVersionAttributesCall(String resourceId, ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = implementationversion;
        
        // create path and map variables
        String localVarPath = "/implementationversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateImplementationVersionAttributesValidateBeforeCall(String resourceId, ImplementationVersion implementationversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateImplementationVersionAttributes(Async)");
        }
        
        // verify the required parameter 'implementationversion' is set
        if (implementationversion == null) {
            throw new ApiException("Missing the required parameter 'implementationversion' when calling updateImplementationVersionAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateImplementationVersionAttributesCall(resourceId, implementationversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @return ImplementationVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ImplementationVersion updateImplementationVersionAttributes(String resourceId, ImplementationVersion implementationversion) throws ApiException {
        ApiResponse<ImplementationVersion> resp = updateImplementationVersionAttributesWithHttpInfo(resourceId, implementationversion);
        return resp.getData();
    }

    /**
     * Partially update a registered implementationversion
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @return ApiResponse&lt;ImplementationVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ImplementationVersion> updateImplementationVersionAttributesWithHttpInfo(String resourceId, ImplementationVersion implementationversion) throws ApiException {
        com.squareup.okhttp.Call call = updateImplementationVersionAttributesValidateBeforeCall(resourceId, implementationversion, null, null);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered implementationversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param implementationversion ImplementationVersion object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateImplementationVersionAttributesAsync(String resourceId, ImplementationVersion implementationversion, final ApiCallback<ImplementationVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateImplementationVersionAttributesValidateBeforeCall(resourceId, implementationversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ImplementationVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
