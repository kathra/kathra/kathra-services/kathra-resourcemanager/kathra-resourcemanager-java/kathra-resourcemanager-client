/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.Assignation;

public class AssignationsClient {
    private ApiClient apiClient;

    public AssignationsClient() {
        this.apiClient = new ApiClient().setUserAgent("AssignationsClient 1.0.0-RC-SNAPSHOT");
    }

    public AssignationsClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public AssignationsClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addAssignation
     * @param assignation Assignation object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addAssignationCall(Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = assignation;
        
        // create path and map variables
        String localVarPath = "/assignations";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addAssignationValidateBeforeCall(Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'assignation' is set
        if (assignation == null) {
            throw new ApiException("Missing the required parameter 'assignation' when calling addAssignation(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addAssignationCall(assignation, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new assignation
     * 
     * @param assignation Assignation object to be created (required)
     * @return Assignation
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Assignation addAssignation(Assignation assignation) throws ApiException {
        ApiResponse<Assignation> resp = addAssignationWithHttpInfo(assignation);
        return resp.getData();
    }

    /**
     * Add a new assignation
     * 
     * @param assignation Assignation object to be created (required)
     * @return ApiResponse&lt;Assignation&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Assignation> addAssignationWithHttpInfo(Assignation assignation) throws ApiException {
        com.squareup.okhttp.Call call = addAssignationValidateBeforeCall(assignation, null, null);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new assignation (asynchronously)
     * 
     * @param assignation Assignation object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addAssignationAsync(Assignation assignation, final ApiCallback<Assignation> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addAssignationValidateBeforeCall(assignation, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteAssignation
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteAssignationCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/assignations/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteAssignationValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteAssignation(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteAssignationCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteAssignation(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteAssignationWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteAssignationWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteAssignationValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered assignation (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteAssignationAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteAssignationValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getAssignation
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getAssignationCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/assignations/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getAssignationValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getAssignation(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getAssignationCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific assignation object
     * 
     * @param resourceId resource's id (required)
     * @return Assignation
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Assignation getAssignation(String resourceId) throws ApiException {
        ApiResponse<Assignation> resp = getAssignationWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific assignation object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;Assignation&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Assignation> getAssignationWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getAssignationValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific assignation object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getAssignationAsync(String resourceId, final ApiCallback<Assignation> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getAssignationValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getAssignations
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getAssignationsCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/assignations";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getAssignationsValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getAssignationsCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible assignations for authenticated user
     * 
     * @return List&lt;Assignation&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Assignation> getAssignations() throws ApiException {
        ApiResponse<List<Assignation>> resp = getAssignationsWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible assignations for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;Assignation&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Assignation>> getAssignationsWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getAssignationsValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<Assignation>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible assignations for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getAssignationsAsync(final ApiCallback<List<Assignation>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getAssignationsValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Assignation>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateAssignation
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateAssignationCall(String resourceId, Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = assignation;
        
        // create path and map variables
        String localVarPath = "/assignations/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateAssignationValidateBeforeCall(String resourceId, Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateAssignation(Async)");
        }
        
        // verify the required parameter 'assignation' is set
        if (assignation == null) {
            throw new ApiException("Missing the required parameter 'assignation' when calling updateAssignation(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateAssignationCall(resourceId, assignation, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @return Assignation
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Assignation updateAssignation(String resourceId, Assignation assignation) throws ApiException {
        ApiResponse<Assignation> resp = updateAssignationWithHttpInfo(resourceId, assignation);
        return resp.getData();
    }

    /**
     * Fully update a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @return ApiResponse&lt;Assignation&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Assignation> updateAssignationWithHttpInfo(String resourceId, Assignation assignation) throws ApiException {
        com.squareup.okhttp.Call call = updateAssignationValidateBeforeCall(resourceId, assignation, null, null);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered assignation (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateAssignationAsync(String resourceId, Assignation assignation, final ApiCallback<Assignation> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateAssignationValidateBeforeCall(resourceId, assignation, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateAssignationAttributes
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateAssignationAttributesCall(String resourceId, Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = assignation;
        
        // create path and map variables
        String localVarPath = "/assignations/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateAssignationAttributesValidateBeforeCall(String resourceId, Assignation assignation, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateAssignationAttributes(Async)");
        }
        
        // verify the required parameter 'assignation' is set
        if (assignation == null) {
            throw new ApiException("Missing the required parameter 'assignation' when calling updateAssignationAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateAssignationAttributesCall(resourceId, assignation, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @return Assignation
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Assignation updateAssignationAttributes(String resourceId, Assignation assignation) throws ApiException {
        ApiResponse<Assignation> resp = updateAssignationAttributesWithHttpInfo(resourceId, assignation);
        return resp.getData();
    }

    /**
     * Partially update a registered assignation
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @return ApiResponse&lt;Assignation&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Assignation> updateAssignationAttributesWithHttpInfo(String resourceId, Assignation assignation) throws ApiException {
        com.squareup.okhttp.Call call = updateAssignationAttributesValidateBeforeCall(resourceId, assignation, null, null);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered assignation (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param assignation Assignation object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateAssignationAttributesAsync(String resourceId, Assignation assignation, final ApiCallback<Assignation> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateAssignationAttributesValidateBeforeCall(resourceId, assignation, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Assignation>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
