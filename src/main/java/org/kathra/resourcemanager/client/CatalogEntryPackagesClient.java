/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.CatalogEntryPackage;

public class CatalogEntryPackagesClient {
    private ApiClient apiClient;

    public CatalogEntryPackagesClient() {
        this.apiClient = new ApiClient().setUserAgent("CatalogEntryPackagesClient 1.0.0-RC-SNAPSHOT");
    }

    public CatalogEntryPackagesClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public CatalogEntryPackagesClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addCatalogEntryPackage
     * @param catalogentrypackage CatalogEntryPackage object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addCatalogEntryPackageCall(CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentrypackage;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addCatalogEntryPackageValidateBeforeCall(CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'catalogentrypackage' is set
        if (catalogentrypackage == null) {
            throw new ApiException("Missing the required parameter 'catalogentrypackage' when calling addCatalogEntryPackage(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addCatalogEntryPackageCall(catalogentrypackage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new catalogentrypackage
     * 
     * @param catalogentrypackage CatalogEntryPackage object to be created (required)
     * @return CatalogEntryPackage
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackage addCatalogEntryPackage(CatalogEntryPackage catalogentrypackage) throws ApiException {
        ApiResponse<CatalogEntryPackage> resp = addCatalogEntryPackageWithHttpInfo(catalogentrypackage);
        return resp.getData();
    }

    /**
     * Add a new catalogentrypackage
     * 
     * @param catalogentrypackage CatalogEntryPackage object to be created (required)
     * @return ApiResponse&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackage> addCatalogEntryPackageWithHttpInfo(CatalogEntryPackage catalogentrypackage) throws ApiException {
        com.squareup.okhttp.Call call = addCatalogEntryPackageValidateBeforeCall(catalogentrypackage, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new catalogentrypackage (asynchronously)
     * 
     * @param catalogentrypackage CatalogEntryPackage object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addCatalogEntryPackageAsync(CatalogEntryPackage catalogentrypackage, final ApiCallback<CatalogEntryPackage> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addCatalogEntryPackageValidateBeforeCall(catalogentrypackage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteCatalogEntryPackage
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteCatalogEntryPackageCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteCatalogEntryPackageValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteCatalogEntryPackage(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteCatalogEntryPackageCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteCatalogEntryPackage(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteCatalogEntryPackageWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteCatalogEntryPackageWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteCatalogEntryPackageValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered catalogentrypackage (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteCatalogEntryPackageAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteCatalogEntryPackageValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntryPackage
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryPackageValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getCatalogEntryPackage(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCatalogEntryPackageCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific catalogentrypackage object
     * 
     * @param resourceId resource's id (required)
     * @return CatalogEntryPackage
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackage getCatalogEntryPackage(String resourceId) throws ApiException {
        ApiResponse<CatalogEntryPackage> resp = getCatalogEntryPackageWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific catalogentrypackage object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackage> getCatalogEntryPackageWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryPackageValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific catalogentrypackage object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackageAsync(String resourceId, final ApiCallback<CatalogEntryPackage> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryPackageValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCatalogEntryPackages
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackagesCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCatalogEntryPackagesValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getCatalogEntryPackagesCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible catalogentrypackages for authenticated user
     * 
     * @return List&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<CatalogEntryPackage> getCatalogEntryPackages() throws ApiException {
        ApiResponse<List<CatalogEntryPackage>> resp = getCatalogEntryPackagesWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible catalogentrypackages for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;CatalogEntryPackage&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<CatalogEntryPackage>> getCatalogEntryPackagesWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getCatalogEntryPackagesValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackage>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible catalogentrypackages for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCatalogEntryPackagesAsync(final ApiCallback<List<CatalogEntryPackage>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCatalogEntryPackagesValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<CatalogEntryPackage>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateCatalogEntryPackage
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryPackageCall(String resourceId, CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentrypackage;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateCatalogEntryPackageValidateBeforeCall(String resourceId, CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateCatalogEntryPackage(Async)");
        }
        
        // verify the required parameter 'catalogentrypackage' is set
        if (catalogentrypackage == null) {
            throw new ApiException("Missing the required parameter 'catalogentrypackage' when calling updateCatalogEntryPackage(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateCatalogEntryPackageCall(resourceId, catalogentrypackage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @return CatalogEntryPackage
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackage updateCatalogEntryPackage(String resourceId, CatalogEntryPackage catalogentrypackage) throws ApiException {
        ApiResponse<CatalogEntryPackage> resp = updateCatalogEntryPackageWithHttpInfo(resourceId, catalogentrypackage);
        return resp.getData();
    }

    /**
     * Fully update a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @return ApiResponse&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackage> updateCatalogEntryPackageWithHttpInfo(String resourceId, CatalogEntryPackage catalogentrypackage) throws ApiException {
        com.squareup.okhttp.Call call = updateCatalogEntryPackageValidateBeforeCall(resourceId, catalogentrypackage, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered catalogentrypackage (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryPackageAsync(String resourceId, CatalogEntryPackage catalogentrypackage, final ApiCallback<CatalogEntryPackage> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateCatalogEntryPackageValidateBeforeCall(resourceId, catalogentrypackage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateCatalogEntryPackageAttributes
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryPackageAttributesCall(String resourceId, CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = catalogentrypackage;
        
        // create path and map variables
        String localVarPath = "/catalogentrypackages/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateCatalogEntryPackageAttributesValidateBeforeCall(String resourceId, CatalogEntryPackage catalogentrypackage, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateCatalogEntryPackageAttributes(Async)");
        }
        
        // verify the required parameter 'catalogentrypackage' is set
        if (catalogentrypackage == null) {
            throw new ApiException("Missing the required parameter 'catalogentrypackage' when calling updateCatalogEntryPackageAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateCatalogEntryPackageAttributesCall(resourceId, catalogentrypackage, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @return CatalogEntryPackage
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CatalogEntryPackage updateCatalogEntryPackageAttributes(String resourceId, CatalogEntryPackage catalogentrypackage) throws ApiException {
        ApiResponse<CatalogEntryPackage> resp = updateCatalogEntryPackageAttributesWithHttpInfo(resourceId, catalogentrypackage);
        return resp.getData();
    }

    /**
     * Partially update a registered catalogentrypackage
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @return ApiResponse&lt;CatalogEntryPackage&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CatalogEntryPackage> updateCatalogEntryPackageAttributesWithHttpInfo(String resourceId, CatalogEntryPackage catalogentrypackage) throws ApiException {
        com.squareup.okhttp.Call call = updateCatalogEntryPackageAttributesValidateBeforeCall(resourceId, catalogentrypackage, null, null);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered catalogentrypackage (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateCatalogEntryPackageAttributesAsync(String resourceId, CatalogEntryPackage catalogentrypackage, final ApiCallback<CatalogEntryPackage> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateCatalogEntryPackageAttributesValidateBeforeCall(resourceId, catalogentrypackage, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CatalogEntryPackage>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
