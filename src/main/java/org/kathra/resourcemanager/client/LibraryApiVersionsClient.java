/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.resourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.LibraryApiVersion;

public class LibraryApiVersionsClient {
    private ApiClient apiClient;

    public LibraryApiVersionsClient() {
        this.apiClient = new ApiClient().setUserAgent("LibraryApiVersionsClient 1.0.0-RC-SNAPSHOT");
    }

    public LibraryApiVersionsClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public LibraryApiVersionsClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addLibraryApiVersion
     * @param libraryapiversion LibraryApiVersion object to be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addLibraryApiVersionCall(LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = libraryapiversion;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addLibraryApiVersionValidateBeforeCall(LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'libraryapiversion' is set
        if (libraryapiversion == null) {
            throw new ApiException("Missing the required parameter 'libraryapiversion' when calling addLibraryApiVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addLibraryApiVersionCall(libraryapiversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add a new libraryapiversion
     * 
     * @param libraryapiversion LibraryApiVersion object to be created (required)
     * @return LibraryApiVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public LibraryApiVersion addLibraryApiVersion(LibraryApiVersion libraryapiversion) throws ApiException {
        ApiResponse<LibraryApiVersion> resp = addLibraryApiVersionWithHttpInfo(libraryapiversion);
        return resp.getData();
    }

    /**
     * Add a new libraryapiversion
     * 
     * @param libraryapiversion LibraryApiVersion object to be created (required)
     * @return ApiResponse&lt;LibraryApiVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<LibraryApiVersion> addLibraryApiVersionWithHttpInfo(LibraryApiVersion libraryapiversion) throws ApiException {
        com.squareup.okhttp.Call call = addLibraryApiVersionValidateBeforeCall(libraryapiversion, null, null);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add a new libraryapiversion (asynchronously)
     * 
     * @param libraryapiversion LibraryApiVersion object to be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addLibraryApiVersionAsync(LibraryApiVersion libraryapiversion, final ApiCallback<LibraryApiVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addLibraryApiVersionValidateBeforeCall(libraryapiversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteLibraryApiVersion
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteLibraryApiVersionCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteLibraryApiVersionValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling deleteLibraryApiVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteLibraryApiVersionCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteLibraryApiVersion(String resourceId) throws ApiException {
        ApiResponse<String> resp = deleteLibraryApiVersionWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Delete a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteLibraryApiVersionWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = deleteLibraryApiVersionValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete a registered libraryapiversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteLibraryApiVersionAsync(String resourceId, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteLibraryApiVersionValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getLibraryApiVersion
     * @param resourceId resource's id (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getLibraryApiVersionCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getLibraryApiVersionValidateBeforeCall(String resourceId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling getLibraryApiVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getLibraryApiVersionCall(resourceId, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a specific libraryapiversion object
     * 
     * @param resourceId resource's id (required)
     * @return LibraryApiVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public LibraryApiVersion getLibraryApiVersion(String resourceId) throws ApiException {
        ApiResponse<LibraryApiVersion> resp = getLibraryApiVersionWithHttpInfo(resourceId);
        return resp.getData();
    }

    /**
     * Retrieve a specific libraryapiversion object
     * 
     * @param resourceId resource's id (required)
     * @return ApiResponse&lt;LibraryApiVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<LibraryApiVersion> getLibraryApiVersionWithHttpInfo(String resourceId) throws ApiException {
        com.squareup.okhttp.Call call = getLibraryApiVersionValidateBeforeCall(resourceId, null, null);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a specific libraryapiversion object (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getLibraryApiVersionAsync(String resourceId, final ApiCallback<LibraryApiVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getLibraryApiVersionValidateBeforeCall(resourceId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getLibraryApiVersions
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getLibraryApiVersionsCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getLibraryApiVersionsValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getLibraryApiVersionsCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve a list of accessible libraryapiversions for authenticated user
     * 
     * @return List&lt;LibraryApiVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<LibraryApiVersion> getLibraryApiVersions() throws ApiException {
        ApiResponse<List<LibraryApiVersion>> resp = getLibraryApiVersionsWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve a list of accessible libraryapiversions for authenticated user
     * 
     * @return ApiResponse&lt;List&lt;LibraryApiVersion&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<LibraryApiVersion>> getLibraryApiVersionsWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getLibraryApiVersionsValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<LibraryApiVersion>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve a list of accessible libraryapiversions for authenticated user (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getLibraryApiVersionsAsync(final ApiCallback<List<LibraryApiVersion>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getLibraryApiVersionsValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<LibraryApiVersion>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateLibraryApiVersion
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateLibraryApiVersionCall(String resourceId, LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = libraryapiversion;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateLibraryApiVersionValidateBeforeCall(String resourceId, LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateLibraryApiVersion(Async)");
        }
        
        // verify the required parameter 'libraryapiversion' is set
        if (libraryapiversion == null) {
            throw new ApiException("Missing the required parameter 'libraryapiversion' when calling updateLibraryApiVersion(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateLibraryApiVersionCall(resourceId, libraryapiversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Fully update a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @return LibraryApiVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public LibraryApiVersion updateLibraryApiVersion(String resourceId, LibraryApiVersion libraryapiversion) throws ApiException {
        ApiResponse<LibraryApiVersion> resp = updateLibraryApiVersionWithHttpInfo(resourceId, libraryapiversion);
        return resp.getData();
    }

    /**
     * Fully update a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @return ApiResponse&lt;LibraryApiVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<LibraryApiVersion> updateLibraryApiVersionWithHttpInfo(String resourceId, LibraryApiVersion libraryapiversion) throws ApiException {
        com.squareup.okhttp.Call call = updateLibraryApiVersionValidateBeforeCall(resourceId, libraryapiversion, null, null);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Fully update a registered libraryapiversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateLibraryApiVersionAsync(String resourceId, LibraryApiVersion libraryapiversion, final ApiCallback<LibraryApiVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateLibraryApiVersionValidateBeforeCall(resourceId, libraryapiversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for updateLibraryApiVersionAttributes
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call updateLibraryApiVersionAttributesCall(String resourceId, LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = libraryapiversion;
        
        // create path and map variables
        String localVarPath = "/libraryapiversions/{resourceId}"
            .replaceAll("\\{" + "resourceId" + "\\}", apiClient.escapeString(resourceId.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "PATCH", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call updateLibraryApiVersionAttributesValidateBeforeCall(String resourceId, LibraryApiVersion libraryapiversion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'resourceId' is set
        if (resourceId == null) {
            throw new ApiException("Missing the required parameter 'resourceId' when calling updateLibraryApiVersionAttributes(Async)");
        }
        
        // verify the required parameter 'libraryapiversion' is set
        if (libraryapiversion == null) {
            throw new ApiException("Missing the required parameter 'libraryapiversion' when calling updateLibraryApiVersionAttributes(Async)");
        }
        
        
        com.squareup.okhttp.Call call = updateLibraryApiVersionAttributesCall(resourceId, libraryapiversion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Partially update a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @return LibraryApiVersion
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public LibraryApiVersion updateLibraryApiVersionAttributes(String resourceId, LibraryApiVersion libraryapiversion) throws ApiException {
        ApiResponse<LibraryApiVersion> resp = updateLibraryApiVersionAttributesWithHttpInfo(resourceId, libraryapiversion);
        return resp.getData();
    }

    /**
     * Partially update a registered libraryapiversion
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @return ApiResponse&lt;LibraryApiVersion&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<LibraryApiVersion> updateLibraryApiVersionAttributesWithHttpInfo(String resourceId, LibraryApiVersion libraryapiversion) throws ApiException {
        com.squareup.okhttp.Call call = updateLibraryApiVersionAttributesValidateBeforeCall(resourceId, libraryapiversion, null, null);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Partially update a registered libraryapiversion (asynchronously)
     * 
     * @param resourceId resource's id (required)
     * @param libraryapiversion LibraryApiVersion object to be updated (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call updateLibraryApiVersionAttributesAsync(String resourceId, LibraryApiVersion libraryapiversion, final ApiCallback<LibraryApiVersion> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = updateLibraryApiVersionAttributesValidateBeforeCall(resourceId, libraryapiversion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LibraryApiVersion>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
